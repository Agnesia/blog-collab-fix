<h1>Kelompok 8 </h1>
<h4> Anggota : </h4>
<ul>
    <li> Abdurrahman Aziz </li>
    <li> Agnesia </li>
    <li> Ahmad Rizal </li>
</ul>

<h4> Tema Project : Blog Edukasi </h4>
<p>Deskripsi singkat : <p>
<p> Blog ini berisi postingan-postingan untuk keperluan edukasi seperti tentang ERD, Pengenalan Java, dan lain-lain.</p>


<h1>Link Video Demo </h1>

<p> Link : https://drive.google.com/drive/folders/1mILZRRsDQOSFbfOUVhIoYU8ypgfJlYh_?usp=sharing </p>
<p> Link Web (Deploy dari Heroku): http://quiet-beach-97528.herokuapp.com </p>
<p> Theme: <b>https://themewagon.com/themes/free-html5-responsive-education-template/</b> </p>

<h1>Library/Package </h1>

<p> Sweet Alert : https://realrashid.github.io/sweet-alert </p>
<p> CKEditor: https://github.com/UniSharp/laravel-ckeditor </p>
<p> DomPDF: https://github.com/barryvdh/laravel-dompdf </p>

