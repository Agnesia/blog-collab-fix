<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('master');
});

Route::get('/show', function () {
    return view('show');
});

/*Route::get('/index', function () {
    return view('index');
});
*/
Route::get('/create', function () {
    return view('create');
});
Route::get('/post/create', 'PostController@create');
Route::get('/post', 'PostController@index');
Route::post('/post', 'PostController@post');
Route::get('/post/{post_id}', 'PostController@show');
Route::get('/post/{post_id}/edit', 'PostController@edit');
Route::put('/post/{post_id}', 'PostController@update');
Route::delete('/post/{post_id}', 'PostController@destroy');
Route::post('/post/{post_id}/like', 'PostController@like');
Route::get('/post/{post_id}/cetak_pdf', 'PostController@cetakPdf');

Auth::routes();
Route::get('/home', 'PostController@index');

 
Route::get('/comments/show' , 'CommentController@create');

Route::post('/post/{post_id}/comment', 'CommentController@store');

//menampilkan
Route::get('/comment', 'CommentController@index');

Route::get('/post/{post_id}/comment/{id}/edit' , 'CommentController@edit');

Route::put('/post/comment/{id}' , 'CommentController@update');

Route::delete('/comment/{id}' , 'CommentController@destroy');

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::get('/create/profile', 'ProfileController@create');

Route::post('/profile', 'ProfileController@store');

Route::get('/profile/{profile_id}/edit', 'ProfileController@edit');

Route::put('/profile/{profile_id}', 'ProfileController@update');
Route::delete('/profile/{profile_id}', 'ProfileController@destroy');
//=================/

