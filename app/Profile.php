<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\LikePosts;
use App\Post;

class Profile extends Model
{
    protected $table = 'profiles';
    protected $fillable = [
        'name', 'bio', 'job', 'photo', 'user_id'
    ];
	public function post(){
		return $this->hasMany('App\Post');
    
    }
    
     public function profile_like(){
     return $this->belongsToMany('App\Post', 'like_posts')->withPivot('value');
    }
    
    public function comment(){
		return $this->hasMany('App\Comment');
    
    }
    
    public function account()
    {
        return $this->belongsTo('App\User'); 
    }

}

 
 
