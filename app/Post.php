<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\LikePosts;

class Post extends Model
{
    protected $table = 'posts';
    protected $fillable = [
        'title', 'context', 'photo', 'profile_id'
    ];
   public function post_like(){
     return $this->belongsToMany('App\Profile', 'like_posts')->withPivot('value');
    }
    public function profile(){
     return $this->belongsTo('App\Profile');
    }
     public function comment(){
     return $this->belongsTo('App\Comment');
    }
}
