<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Post;
use App\Profile; 
use RealRashid\SweetAlert\Facades\Alert;
use Carbon\Carbon;
use App\Comment;
use PDF;
use File;
use Storage;
use Illuminate\Support\Facades\Auth;
class PostController extends Controller
{
	 public function __construct()
    {
        $this->middleware('auth');
    }
    
    function create() {
		$pr = Profile::where('user_id', Auth::id())->get();
		if ($pr->count() >0) {
			return view('create') ;
		}
		else {
			Alert::error('Error', 'Mohon lengkapi profile');	
			return redirect('/post');
		}
		
	}
	//using one to many
	 function post(Request $request) {
		$request->validate(
		[
        'title' => 'required|unique:posts|max:255',
        'context' => 'required|max:3000',
        'file' => 'required'
		]);
		
		//upload foto ke database
		$file = $request->file('file');
        $ext = $file->getClientOriginalExtension();
        $newName = rand(100000,1001238912).".".$ext;
        $file->move('uploads/file',$newName);
		$pr = Profile::where('user_id', Auth::id())->first();
		
		if (Post::create([
			'title' => $request['title'],
            'context' => $request['context'],
            'photo' => $newName,
			'profile_id' => $pr->id
        ])) {
			Alert::success('Success', 'Post success');	
			
		}
		else {
			Alert::error('Failed', 'Post failed');	
		}
		return redirect ('/post');
	}
	
	function index() {
		$post = Post::all();
		foreach ($post as $p) {
			$p['vote'] = $p->post_like->sum('pivot.value');
			$p['comment']=Comment::where('post_id', $p->id)->count();
			//batesin cuma 5 enter pertama yang muncul dan panjangnya 100 karakter aja 
			$length_enter = substr_count($p['context'], '<p>'); 
			if ($length_enter>3) {
				$words = explode('<p>', $p['context']);
				$first_three = array_slice($words, 0, 3);
				$p['context'] = implode('<p>', $first_three);
			}
			else if (strlen ($p['context'])>200) {
				$p['context']= substr($p['context'], 0, 200) . "...";
			} 
			$datetime = Carbon::createFromFormat('Y-m-d H:i:s', $p['created_at']);

			$datetime->setTimezone('Asia/Jakarta');
			$p['created_at']=$datetime;
		}
		$profile= Profile::where('user_id', Auth::id())->first();
		
		return view('index',compact('post', 'profile'));
	}
	//using many to many
	function show($id) {
		
		$post = Post::find($id);
		$post['vote'] = $post->post_like->sum('pivot.value');
		
		$comments= Comment::where('post_id', $id)->get();
		$post['comment']=$comments->count();
		$profile= Profile::where('user_id', Auth::id())->first();
		foreach ($comments as $c) {
		   
			$c['photo'] = $c->profile->photo;
			$datetime = Carbon::createFromFormat('Y-m-d H:i:s', $c['created_at']);

			$datetime->setTimezone('Asia/Jakarta');
			$c['created_at']=$datetime;
		}
		return view('show', compact('post', 'comments', 'profile')) ;
	}
	
	function edit($id, Request $request) {
		$post = Post::find($id);;
		return view('edit', compact('post')) ;
	}
	
	function update($id, Request $request) {
		$post = Post::find($id);
		$request->validate([
        'title' => 'required|max:255|unique:posts,title,'.$id,
        'context' => 'required|max:3000'
		]);
		
		$post->title = $request->title;
		$post->context = $request->context;
		if($request->hasFile('file')){ 
			$file = $request->file('file');
			$ext = $file->getClientOriginalExtension();
			$newName = rand(100000,1001238912).".".$ext;
			$file->move('uploads/file',$newName);
			File::delete(public_path() . '/uploads/file/'.$post->photo);
			
			$post->photo = $newName;
		}
		if ($post->save()) {
			Alert::success('Berhasil', 'Update data berhasil');	
		}
		else {
			Alert::error('Gagal', 'Update data gagal');	
		}
		return redirect('/post');
	}
	
	function destroy($id, Request $request) {
		$post = Post::find($id);
		$url = $post->photo;
		if ($post->delete()) {
			File::delete(public_path() . '/uploads/file/'.$url);
			Alert::success('Berhasil', 'Postingan berhasil dihapus.');	
			
		}
		else {
			Alert::error('Gagal', 'Gagal menghapus data');	
		}
		return redirect('/post');
	}
	
	function like ($id) {
		$post = Post::find($id);$profile= Profile::where('user_id', Auth::id())->first();
		$post->post_like()->attach($profile->id, ['value'=>1 ]);
		Alert::success('Berhasil', 'Kamu memberikan like pada postingan ini');	
		return redirect('/post');
       
	}
	
	public function cetakPdf($id)
    {
		$post = Post::find($id);
 
    	$pdf = PDF::loadview('pegawai_pdf',['post'=>$post]);
    	
    	return $pdf->download('export.pdf');
    }
}
