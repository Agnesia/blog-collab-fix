<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use App\Comment;
use App\Post;
use App\Profile;
use Alert;
use Illuminate\Support\Facades\Auth;
class CommentController extends Controller
{
	 public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function create() {
        return view('comments.show');
    }

    public function store(Request $request, $id){
       $request->validate([
           'isi' => 'required'
       ]);
		$post = Post::findOrFail($id);
		$comments= Comment::where('post_id', $post['id'])->get();
     
		$profile= Profile::where('user_id', Auth::id())->first();
     
        //Eloquent
       $comment = new Comment;
       $comment->isi = $request["isi"];
       $comment->post_id = $id;
       $comment->profile_id = $profile->id;
       if ($comment->save()) {
			Alert::success('Success', 'Comment Posted');	
		}
       
        //batas
	
	return redirect('/post/'. $id) ->with( 'comments', $comments)->with ('post', $post);
    }
    //hitung comment
    
   

    public function index(){
        $comments= Comment::all();
        
        return view('comments.index' , compact('comments'));
    }

    public function edit($id, $id2) {
       //eloquent
        $comment = Comment::find($id2);
        echo ($id);
        echo ($comment);
        $post_id['id']=$id;
        return view('comments.edit', compact('comment'));
    }

    public function update($id, Request $request){
		  //eloquent
		/*$comments = Comment::where('id' , $id)->update([
			"nama" => $request["nama"],
			"isi" => $request["isi"],
		   // "created_at"=> $request["created_at"]
		]);*/
		$comment = Comment::find($id);
		
		$comment->isi = $request->isi;
		
		if ($comment->save()) {
			Alert::success('Success', 'Comment Updated');	
		}
		$post = Post::findOrFail($comment['post_id']);
		$comments= Comment::where('post_id', $post['id'])->get();
       
		//return redirect('/show', compact('comment', 'post'));
		return redirect('/post/'. $post['id']) ->with( 'comments', $comments)->with ('post', $post);

    }

    public function destroy($id){
       //eloquent
        $comm= Comment::findOrFail( $id);
        $post = Post::findOrFail($comm->post_id);
        if ($comm->delete()) {
			Alert::success('Success', 'Comment Deleted');	
		}
        
        return redirect('/post/'. $post['id'])->with( 'comments', $comm)->with ('post', $post);
    }
}
