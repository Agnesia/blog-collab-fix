<?php

namespace App\Http\Controllers;

use Illuminate\Console\Scheduling\Event;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;
use App\Profile;
use App\User;
use File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ProfileController extends Controller
{
     
     public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        return view('profiles.index',[
            'profiles' => Profile::all(),
            ]);
    }

    public function create()
    {
        return view('profiles.create');
    }

    public function store(Request $request)
    {
        $file =  $request->file('file');
        $ext = $file->getClientOriginalExtension();
        $img = rand(100000,1001238912).".".$ext;
        $file->move('uploads/file',$img);    
        
        $data = $request->all();
		$data['name'] = $request['name'];
		$data['job'] = $request['job'];
		$data['bio'] = $request['bio'];
        $data['user_id'] = $request['id'];


        Profile::create([
			'name' => $data['name'],
            'job' => $data['job'],
            'bio' => $data['bio'],
            'photo'=> $img,
			'user_id' => Auth::id()
        ]);
		
		Alert::success('Success', 'Profile Added');	
        return redirect ('/post');

    }

    public function show($id)
    { 
        $profile = Profile::find($id);
        return view('profiles.show',[
            'profile' => $profile
            ]);
    }

    public function edit($id)
    {
        return view('profiles.edit',[
            'profile' => Profile::find($id),
            ]);
    }


    public function update(Request $request, $id)
    {   
        $acc = User::find(2);
        $profile = Profile::find($id);
        $profile->name = $request->name;
        $profile->job = $request->job;
        $profile->bio = $request->bio;
        $profile->user_id = Auth::id();
        $file =  $request->file('file');
        if($file!=null){
			
			$ext = $file->getClientOriginalExtension();
			$img = rand(100000,1001238912).".".$ext;
			$file->move('uploads/file/',$img);  
			File::delete(public_path() . '/uploads/file/'.$profile->photo);
			$profile->photo = $img;
            // upload the file
        }
		if ($profile->save()) {
			Alert::success('Success', 'Profile Updated');	
		}
		return redirect('/post') ;
    }
  
    public function destroy($id)
    {
        $profile = Profile::find($id);
        $user =User::find(Auth::user()->id);
        $profile->delete();
		Auth::logout();
		if ($user->delete()) {
			Alert::success('Success', 'Your account has been deleted! See you again.');	
			return redirect('/');
		}
		else  {
			Alert::error('Failed', 'Please try again');	
			return redirect('/post');
		}
        
    }
        
    
}
