<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = "comments";

	protected $fillable = [
        'title', 'context', 'photo', 'summary', 'post_id'
    ];
    public function post() {
        return $this->belongsTo('App\Post');

    }
    
     public function profile(){
     return $this->belongsTo('App\Profile');
    }
    
	
	 
    
}
