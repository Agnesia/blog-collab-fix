	<!DOCTYPE html>
	<html lang="zxx" class="no-js">
	<head>
		<!-- Mobile Specific Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Favicon-->
		<link rel="shortcut icon" href="{{asset ('/education-master/img/fav.png')}}">
		<!-- Author Meta -->
		<meta name="author" content="colorlib">
		<!-- Meta Description -->
		<meta name="description" content="">
		<!-- Meta Keyword -->
		<meta name="keywords" content="">
		<!-- meta character set -->
		<meta charset="UTF-8">
		<!-- Site Title -->
		<title>Education</title>
		<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet"> 
			<!--
			CSS
			
			============================================= -->
			<link rel="stylesheet" href="{{asset ('/education-master/css/linearicons.css')}}">
			
			<link rel="stylesheet" href="{{asset ('/education-master/css/font-awesome.min.css')}}">
			<link rel="stylesheet" href="{{asset ('/education-master/css/bootstrap.css')}}">
			<link rel="stylesheet" href="{{asset ('/education-master/css/magnific-popup.css')}}">
			<link rel="stylesheet" href="{{asset ('/education-master/css/nice-select.css')}}">							
			<link rel="stylesheet" href="{{asset ('/education-master/css/animate.min.css')}}">
			<link rel="stylesheet" href="{{asset ('/education-master/css/owl.carousel.css')}}">			
			<link rel="stylesheet" href="{{asset ('/education-master/css/jquery-ui.css')}}">			
			<link rel="stylesheet" href="{{asset ('/education-master/css/main.css')}}">
			<link rel="stylesheet" href="{{asset ('/css/customize.css')}}">
		</head>
		<body>	
		  @include('layouts.partials.header')
			
			
			<!-- start banner Area -->
			<section class="banner-area relative" id="home">	
				<div class="overlay overlay-bg"></div>
				<div class="container">				
					<div class="row d-flex align-items-center justify-content-center">
						<div class="about-content col-lg-12">
							<h1 class="text-white">Education</h1>
	
							@yield ('banner')
						</div>	
					</div>
				</div>
			</section>
<!-- End banner Area -->					  
			
			@yield('content')
				
			<!-- End post-content Area -->
			
			<!-- start footer Area -->		
			@include ('footer')
			<!-- End footer Area -->	

			<script src="{{asset ('/education-master/js/vendor/jquery-2.2.4.min.js')}}"></script>
			
			<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
			<script src="{{asset ('/education-master/js/vendor/bootstrap.min.js')}}"></script>			
			<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
  			<script src="{{asset ('/education-master/js/easing.min.js')}}"></script>			
			<script src="{{asset ('/education-master/js/hoverIntent.js')}}"></script>
			<script src="{{asset ('/education-master/js/superfish.min.js')}}"></script>	
			<script src="{{asset ('/education-master/js/jquery.ajaxchimp.min.js')}}"></script>
			<script src="{{asset ('/education-master/js/jquery.magnific-popup.min.js')}}"></script>	
    		<script src="{{asset ('/education-master/js/jquery.tabs.min.js')}}"></script>						
			<script src="{{asset ('/education-master/js/jquery.nice-select.min.js')}}"></script>	
			<script src="{{asset ('/education-master/js/owl.carousel.min.js')}}"></script>									
			<script src="{{asset ('/education-master/js/mail-script.js')}}"></script>	
			<script src="{{asset ('/education-master/js/main.js')}}"></script>	
			<script src="{{asset ('/ckeditor/ckeditor.js')}}"></script>
			
			@include('sweetalert::alert')
			@stack('script')
		</body>
	</html>
