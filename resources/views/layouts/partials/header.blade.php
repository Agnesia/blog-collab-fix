<header id="header" id="home">
    <div class="header-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-sm-6 col-8 header-top-left no-padding">
                    <ul>
                      <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                      <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                      <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                    </ul>			
                </div>
                <div class="col-lg-6 col-sm-6 col-4 header-top-right no-padding">
                  @if (Route::has('login'))
                  <div class="top-right links">
                      @auth
						  <a href="{{ url('/post') }}">{{Auth::user()->name}}</a>
                          <a href="{{ url('/logout') }}">Logout</a>
                      @else
                          <a href="{{ route('login') }}"><b>Login</b></a>
  
                          @if (Route::has('register'))
                              <a href="{{ route('register') }}">Register</a>
                          @endif
                      @endauth
                  </div>
                  @endif			
                </div>
            </div>			  					
        </div>
  </div>
  </header><!-- #header -->
