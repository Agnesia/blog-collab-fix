@extends ('master')
@section('banner')
	<p class="text-white link-nav">Posts<span class="lnr lnr-arrow-right"></span>Show</p>
@endsection
@section ('content')

			<!-- Start post-content Area -->
			<section class="post-content-area single-post-area">
				<div class="container">
					<div class="row">
						<div class="col-lg-8 posts-list">
							<div class="single-post row">
								<div class="col-lg-12">
									<div class="feature-img">
										<!--img class="img-fluid" src="{{asset ('/education-master/img/blog/feature-img1.jpg')}}" alt=""-->
										<img class="img-fluid" src="{{ url('uploads/file/'.$post->photo) }}" alt="">
									</div>									
								</div>
								<div class="col-lg-3  col-md-3 meta-details">
									<ul class="tags">
										<li><a href="#">Food,</a></li>
										<li><a href="#">Technology,</a></li>
										<li><a href="#">Politics,</a></li>
										<li><a href="#">Lifestyle</a></li>
									</ul>
									<div class="user-details row">
										<p class="user-name col-lg-12 col-md-12 col-6"><a href="#">{{ $post->profile->name}}</a> <span class="lnr lnr-user"></span></p>
										<p class="date col-lg-12 col-md-12 col-6"><a href="#">{{ $post->created_at->format('d M, Y') }}</a> <span class="lnr lnr-calendar-full"></span></p>
										<p class="view col-lg-12 col-md-12 col-6"><a href="#">{{ $post->vote }}</a> <span class="lnr lnr-heart"></span></p>
										<p class="comments col-lg-12 col-md-12 col-6"><a href="#">{{ $post->comment }} Comments</a> <span class="lnr lnr-bubble"></span></p>
										<ul class="social-links col-lg-12 col-md-12 col-6">
											<li><a href="#"><i class="fa fa-facebook"></i></a></li>
											<li><a href="#"><i class="fa fa-twitter"></i></a></li>
											<li><a href="#"><i class="fa fa-github"></i></a></li>
										</ul>																				
									</div>
								</div>
								<div class="col-lg-9 col-md-9">
									<h3 class="mt-20 mb-20">{{$post->title}}</h3>
									<p class="excert">
										{!! $post->context !!}MCSE boot camps have its supporters and its detractors. Some people do not understand why you should have to spend money on boot camp when you can get the MCSE study materials yourself at a fraction.
									</p>
									
								</div>
								<div class="col-lg-12">
									<div class="quotes">
										MCSE boot camps have its supporters and its detractors. Some people do not understand why you should have to spend money on boot camp when you can get the MCSE study materials yourself at a fraction of the camp price. However, who has the willpower to actually sit through a self-imposed MCSE training.										
									</div>
									<div class="row mt-30 mb-30">
										<div class="col-6">
											<img class="img-fluid" src="{{asset ('/education-master/img/blog/post-img1.jpg')}}" alt="">
										</div>
										<div class="col-6">
											<img class="img-fluid" src="{{asset ('/education-master/img/blog/post-img2.jpg')}}" alt="">
										</div>	
										<div class="col-lg-12 mt-30">
											<p>
												MCSE boot camps have its supporters and its detractors. Some people do not understand why you should have to spend money on boot camp when you can get the MCSE study materials yourself at a fraction of the camp price. However, who has the willpower.
											</p>
											<p>
												MCSE boot camps have its supporters and its detractors. Some people do not understand why you should have to spend money on boot camp when you can get the MCSE study materials yourself at a fraction of the camp price. However, who has the willpower.
											</p>											
										</div>									
									</div>
								</div>
								<div class = "row col-lg-12 d-flex justify-content-end ">
									@if ($post->vote ==0)
								<form action="/post/{{$post->id}}/like" method="POST">
								@csrf
									@method ('POST')
									<button type="submit" class="btn genric-btn primary mx-2">Like</button>
									
								</form>
								@else 
								<button type="submit" class="btn genric-btn primary mx-2" disabled>Like</button>
								@endif
								<a href = "/post/{{$post->id}}/edit" class="btn genric-btn primary mx-2">Edit</a>
								<a href="/post/{{$post->id}}/cetak_pdf" class="btn genric-btn primary mx-2">Cetak PDF</a> 
								<form action="/post/{{$post->id}}" method="post" >
					
									@csrf
									@method ('DELETE')
								 <button class="btn genric-btn primary mx-2" type="submit" >Delete</button>
								</form>
								
								</div>
							</div>
							
							<div class="comments-area">
    
								<h4> {{$comments->count()}} Comments</h4>
								@if(session('success'))
								<div class="alert alert-success">
								{{ session('success')}}
								</div>
								@endif
								@foreach($comments as $key => $comment)
								
								<div class="comment-list">
									<div></div>
									<div class="single-comment justify-content-between d-flex">
										<div class="user justify-content-between d-flex">
											<div class="thumb">
												<img id="photo" style = "width:100px; height: 100px" src="{!! asset('/uploads/file/' . $comment->profile->photo) !!}" />
											</div>
											<div class="desc">
												<h5><a href="#">{{$comment->profile->name}}</a></h5>
												<p class="">{{$comment->created_at}}
													
												  </p>
												<p class="comment">
													{{$comment->isi}}
												</p>
											</div>
										</div>
							<!--Button-->
										<div style="display: flex">
										@if ($profile->id == $comment->profile->id)
										<div class="reply-btn mx-2">
											<a href="/post/{{$post->id}}/comment/{{$comment->id}}/edit" class="btn btn-reply ">Edit</a> 
											
										</div>
										<div class="reply-btn">
											
											<form action="/comment/{{$comment->id}}" method="post">
												@csrf
												@method('DELETE')
											<input type="submit" value="Delete" class="btn btn-reply ">
											</form>
											
										</div>
										@endif
										</div>
									</div>
								</div>
								@endforeach
							</div>
							<div class="comment-form">
								<h4>Leave a Comment</h4>
								<form role="form" action="/post/{{$post->id}}/comment" method="POST">
									@csrf
									
									<div class="form-group">
										<textarea class="form-control mb-10" rows="5" name="isi" id="isi" placeholder="Messege" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Comment'" required=""></textarea>
									</div>
									<button type="submit" class="primary-btn text-uppercase">Post Comment</button> 	
								</form>
							</div>
						</div>
						@include('sidebar')
					</div>
				</div>
			</section>
			
			<!-- End post-content Area -->
@endsection




