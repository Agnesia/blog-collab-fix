
@extends ('master')
@section('banner')
	<p class="text-white link-nav">Profile<span class="lnr lnr-arrow-right"></span>Edit your Profile</p>
@endsection
@section ('content')

<div class="card card-primary">
	<div class="card-header my-2">
		
	<h2 class ="popular-title">Edit Post</h2>
	</div>
	</div>
 
	<form role="form" action="/post/{{$post->id}}" method="POST" enctype="multipart/form-data">
		@csrf
		@method ('PUT')
		<div class="card-body">
			<div class="form-group">
				<label for="title">Title</label>
				<input type="text" class="form-control" id="title" name="title" value = "{{old ('title', $post->title)}}">
				@error ('title')
				<div class="alert alert-danger">{{$message}}</div>
				@enderror
			</div>
			<div class="form-group">
				<label for="body">Isi</label>
				<textarea class="form-control" id="context" name="context">{!!$post->context !!}</textarea>
				@error ('context')
					<div class="alert alert-danger">{{$message}}</div>
				@enderror
			</div>
			 <div class="form-group">
				<label class="col-form-label">Upload File</label><br/>
					<input type="file" class="form-control" id="file" name="file" onchange="loadFile(event)">	
			</div>
			<div class="form-group">
				<img id="output" class="img-fluid"/>
			</div>
		</div>
	<!-- /.card-body -->

		<div class="card-footer">
			<button type="submit" class="btn btn-primary">Submit</button>
		</div>
	</form>
</div>
@endsection
@push ('script')
<script>
	//text editor
	var options = {
		filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
		filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
		filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
		filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
	};
	CKEDITOR.replace('context', options);
	
	//tampilkan foto sebelumnya
	var sites = {!! json_encode($post->toArray()) !!};
	var output = document.getElementById('output');
	document.getElementById('output').src = "/uploads/file/".concat(sites['photo'])
	output.onload = function() {
      URL.revokeObjectURL(output.src) // free memory
	}
	
	//setelah upload file tp blm di save, tampil preview
    var loadFile = function(event) {
    var output = document.getElementById('output');
	output.src = URL.createObjectURL(event.target.files[0]);
    output.onload = function() {
      URL.revokeObjectURL(output.src) // free memory
    }
  };	
</script>
@endpush
