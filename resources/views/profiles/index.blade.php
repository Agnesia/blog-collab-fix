@extends('dashboard')

@section('main')
<h4>Profile</h4>
<div>
    <div class="row">
        <div class="col-sm-6">
    <form>
        @foreach($profiles as $profile)
        <a href="/profile/{{$profile->id}}/edit"" class="btn btn-primary">Edit Profile</a>
        <div class="card-body">
            
            <div class="form-group">
                <input type="hidden" class="form-control" name="name" id="name" value="{{ $profile->id }}" placeholder="Enter name" disabled>
                <label for="name">Full Name</label>
                <input type="text" class="form-control" name="name" id="name" value="{{ $profile->name }}" placeholder="Enter name" disabled>
                </div>
                <div class="form-group">
                <label for="job">Occupation</label>
                <input type="text" class="form-control" name="job" id="job" value="{{ $profile->job }}" placeholder="Enter occupation" disabled>
                </div>
                <div class="form-group">
                <label for="bio">Bio</label>
                <textarea class="form-control" rows="3" name="bio" placeholder="Enter ..." disabled>{{ $profile->bio }}</textarea>
                </div>
                <div class="form-group">
                <label for="image">Image</label>
                <div class="input-group">
                    <div class="custom-file">
                    <input type="file" class="custom-file-input" name="image" value="{{ asset('public/materials/'.$profile->image) }}" id="image" disabled>
                    <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                    </div>
                    <div class="input-group-append">
                    <span class="input-group-text">Upload</span>
                    </div>
                </div>
            </div>
            
        </div> 
        <!-- /.card-body -->

        @endforeach
    </form>
        </div>
        </div>
</div>
@endsection