@extends('master')

@section('content')
<h2>Add Profile</h2>
<div>
    <div class="row">
        <div class="col-sm-6">
    <form action="/profile" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="card-body">
            <div class="form-group">
            <label for="name">Full Name</label>
            <input type="text" class="form-control" name="name" id="name" placeholder="Enter name">
            </div>
            <div class="form-group">
            <label for="job">Occupation</label>
            <input type="text" class="form-control" name="job" id="job" placeholder="Enter occupation">
            </div>
            <div class="form-group">
            <label for="bio">Bio</label>
            <textarea class="form-control" rows="3" name="bio" placeholder="Enter bio"></textarea>
            </div>
            <div class="form-group">
            <label for="image">Image</label>
            <div class="input-group">
                <div class="custom-file">
                <!--input type="file" class="custom-file-input" name="image" id="image"-->
					<input type="file" class="form-control" id="email" name="file" onchange="loadFile(event)">	
						 
					<!--label class="custom-file-label" for="image">Choose file</label-->
                </div>
                
            </div>
            <div class="form-group">
					
						<img id="output" class="img-fluid"/>

			</div>
            
        </div> 
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Add</button>
        </div>
    </form>
    </div>
    </div>
</div>
@endsection
@push ('script')
<script>
var loadFile = function(event) {
    var output = document.getElementById('output');
    output.src = URL.createObjectURL(event.target.files[0]);
    output.onload = function() {
      URL.revokeObjectURL(output.src) // free memory
    }
  };
</script>
@endpush
