@extends('master')
@section('banner')
	<p class="text-white link-nav">Profile<span class="lnr lnr-arrow-right"></span>Edit your Profile</p>
@endsection
@section('content')
<div class="title text-center my-2">
	<h2 class ="popular-title">Your Data</h2>
</div>
<div>
    <div class="row">
		
        <div class="col-sm-6">
        <form action="/profile/{{$profile->id}}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            
            <div class="card-body">
                <div class="form-group">
                <input type="hidden" class="form-control" name="name" id="name" value="{{ $profile->id }}" placeholder="Enter name" disabled>
                <label for="name">Full Name</label>
                <input type="text" class="form-control" value="{{$profile->name}}" name="name" id="name" placeholder="Enter name">
                </div>
                <div class="form-group">
                <label for="job">Occupation</label>
                <input type="text" class="form-control" value="{{$profile->job}}" name="job" id="job" placeholder="Enter occupation">
                </div>
                <div class="form-group">
                <label for="bio">Bio</label>
                <textarea class="form-control" rows="3" name="bio" placeholder="Enter ...">{{$profile->bio}}</textarea>
                </div>
                <div class="form-group">
                <label for="image">Image</label>
                <div class="input-group">
					<input type="file" class="form-control" id="file" name="file" onchange="loadFile(event)">	
                    <!--div class="custom-file">
                    <input type="file" class="custom-file-input" name="image" id="image">
                    <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                    </div>
                    <div class="input-group-append">
                    <span class="input-group-text">Upload</span>
                    </div-->
                </div>
                <div class="form-group">
					<img id="output" class="img-fluid"/>
				</div>
                
                <input type="hidden" class="form-control" name="name" id="name" value="{{ $profile->user_id }}" placeholder="Enter name" disabled>
            </div> 
            <!-- /.card-body -->

            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
        </div>
    </div>
    <div class="col-sm-6 d-flex justify-content-end">
    <form action="/profile/{{$profile->id}}" method="post">
				@csrf
				@method('DELETE')
				<input type="submit" value="Delete Account" class="primary-btn">
			</form>
			</div>
</div>
@endsection
@push ('script')
<script>
//tampilkan foto sebelumnya
	var sites = {!! json_encode($profile->toArray()) !!};
	var output = document.getElementById('output');
	document.getElementById('output').src = "/uploads/file/".concat(sites['photo'])
	output.onload = function() {
      URL.revokeObjectURL(output.src) // free memory
	}
	
	//setelah upload file tp blm di save, tampil preview
    var loadFile = function(event) {
    var output = document.getElementById('output');
    output.src = URL.createObjectURL(event.target.files[0]);
    output.onload = function() {
      URL.revokeObjectURL(output.src) // free memory
    }
  };	
</script>
@endpush
