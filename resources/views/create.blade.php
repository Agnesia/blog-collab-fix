
@extends ('master')
@section('banner')
	<p class="text-white link-nav">Post<span class="lnr lnr-arrow-right"></span>Create New Post</p>
@endsection
@section ('content')

		<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">What do you want to share?</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              
              <form action="/post" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="title">Title</label>	
                    <input type="text" class="form-control" id="title" name="title" placeholder="Enter title">
                    @error ('title')
                    <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="body">Context</label><br>
                    <textarea class="form-control" rows="10" id="context" name="context" placeholder="Enter context" ></textarea>
                    
					<span class="form-bar"></span>
                    @error ('context')
                    <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                  </div>
                   <div class="form-group">
					<label class="col-form-label">Upload File</label><br/>
						<input type="file" class="form-control" id="email" name="file" onchange="loadFile(event)">	
							@error ('file')
								<div class="alert alert-danger">{{$message}}</div>
							@enderror				
					</div>
					<div class="form-group">
					
						<img id="output" class="img-fluid"/>

					</div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
@endsection
@push ('script')
<script>
  CKEDITOR.replace('context');
 
  var loadFile = function(event) {
    var output = document.getElementById('output');
    output.src = URL.createObjectURL(event.target.files[0]);
    output.onload = function() {
      URL.revokeObjectURL(output.src) // free memory
    }
  };
</script>    
@endpush
