@extends ('master')
@section('banner')
	<p class="text-white link-nav">Comment<span class="lnr lnr-arrow-right"></span>Edit</p>
@endsection
@section ('content')
		<!--CommentForm-->	
		<div class="title text-center my-2">
		<h2 class ="popular-title">Edit your Comment</h2>
		</div>
		<div class="col">
		<form role="form" action="/post/comment/{{$comment->id}}" method="POST">
				@csrf
                @method('PUT')
			
				<div class="form-group">
					<textarea type="textarea" class="form-control mb-10" rows="5" name="isi" id="isi" placeholder="Messege" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Comment'" >{{ old('isi', $comment->isi) }}</textarea>
				</div>
				
				<button type="submit" class="primary-btn text-uppercase">Post Comment</button> 	
			</form>
		</div>
@endsection		
		

