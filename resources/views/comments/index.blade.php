@extends('comments.show')

@section('index')
<!--Hitung Comment -->
<?php
    $count = DB::table('comments')->count();
    
    ?>

<!--Comment List Area-->
<div class="comments-area">
    
    <h4> {{$count}} Comments</h4>
    @if(session('success'))
    <div class="alert alert-success">
    {{ session('success')}}
    </div>
    @endif
    @foreach($comments as $key => $comment)
    
    <div class="comment-list">
        <div></div>
        <div class="single-comment justify-content-between d-flex">
            <div class="user justify-content-between d-flex">
                <div class="thumb">
                    <img src="{{asset ('/education-master/img/blog/c1.jpg')}}" alt="">
                </div>
                <div class="desc">
                    <h5><a href="#">{{$comment->nama}}</a></h5>
                    <p class="">{{$comment->created_at}}
                        
                      </p>
                    <p class="comment">
                        {{$comment->isi}}
                    </p>
                </div>
            </div>
<!--Button-->
            <div style="display: flex">
            <div class="reply-btn mx-2">
                <a href="/comment/{{$comment->id}}/edit" class="btn-reply text-uppercase">edit</a> 
            </div>
            <div class="reply-btn">
                
                <form action="/comment/{{$comment->id}}" method="post">
                    @csrf
                    @method('DELETE')
                <input type="submit" value="Delete" class="btn btn-reply ">
                </form>
                
            </div>
            </div>
        </div>
    </div>

    @endforeach
    
    
                                                 				
</div>
@endsection
