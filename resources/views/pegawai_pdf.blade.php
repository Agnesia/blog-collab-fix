<html>
<head>
	<title>Laporan PDF Laravel</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	
	<center>
		<h3>{{$post->photo}}</h4>
		<h6><a target="_blank" href="https://www.education.com/pdf">https://www.education.com/pdf</a></h5>
	</center>
	<img src="{{ public_path() . '/uploads/file/' . $post->photo }}">
	<br><br>
	<p>{!! $post->context !!}</p>
	
</body>
</html>
